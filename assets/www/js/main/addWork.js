var addWork = {
	init:function(){
		"use strict";

		$('.add-work-information').css({'height':$(window).height() - 40 + 'px'});
	},
	openPage:function(){
		"use strict";

		$('.page').hide();
		$('#add-work').show();

		new IScroll('#add-work-wrapper');
		document.addEventListener('touchmove',function(event){event.preventDefault()},false);
	}
};