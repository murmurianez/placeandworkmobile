var camera = {
	capturePhoto:function(){
		navigator.camera.getPicture(camera.uploadPhoto,null,{
			sourceType: Camera.PictureSourceType.CAMERA,
			camareDirection: Camera.Direction.BACK,
			destinationType: Camera.DestinationType.DATA_URL,
			encodingType: Camera.EncodingType.JPEG,
			quality: 75,
			targetWidth: 640,
			targetHeight: 480,
			saveToPhotoAlbum:false,
			correctOrientation:true
		})
	},

	uploadPhoto:function(data){
		$('#photo').attr('src',"data:image/jpeg;base64," + data);
	}
};