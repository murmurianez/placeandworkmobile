var router = {
	element:document.querySelector('.pages'),

    routes:function(){
		var data = event.target.dataset.link;
		switch(data){
			case 'menu':(function(){
					if(router.element.style['left'] === '0px'){
						router.element.style['left'] = '100px';
					}else{
						router.element.style['left'] = '0px';
					}
				})(); break;
			case 'main':            main.openPage(); break;
			case 'add-work':        addWork.openPage(); break;
			case 'my-profile':      myProfile.openPage(); break;
			case 'work-describe':   workDescribe.openPage(); break;
			case 'profession':      profession.openPage(); break;
			case 'options':         options.openPage(); break;
		}
    },

	pointOwner:function(triangle,point){
	//Функция проверяет принадлежность точки треугольнику
	//Получаем координаты вершин треугольника и координаты точки
		"use strict";
		var x1 = triangle.x1;
		var y1 = triangle.y1;
		var x2 = triangle.x2;
		var y2 = triangle.y2;
		var x3 = triangle.x3;
		var y3 = triangle.y3;
		var xP = point.x;
		var yP = point.y;

		var a = (x1 - xP) * (y2 - y1) - (x2 - x1) * (y1 - yP);
		var b = (x2 - xP) * (y3 - y2) - (x3 - x2) * (y2 - yP);
		var c = (x3 - xP) * (y1 - y3) - (x1 - x3) * (y3 - yP);

		return (a < 0 && b < 0 && c < 0) || (a >= 0 && b >= 0 && c >= 0);
	},

	point:{},

	init:function(){
		"use strict";

		main.openPage();

        var button = document.querySelectorAll('.button');

        for(var i = 0; i < button.length; i++){
            button[i].addEventListener('touchstart',router.routes,false);
        }



		var slide = document.querySelector('.pages');

        slide.addEventListener('touchstart',function(event){
			router.point.x = event.targetTouches[0].pageX;
			router.point.y = event.targetTouches[0].pageY;
        },false);

        slide.addEventListener('touchmove',function(event){

			if(router.pointOwner({
					x1:0,
					y1:0,
					x2:0,
					y2:window.innerHeight,
					x3:router.point.x,
					y3:router.point.y
				},
				{
					x:event.changedTouches[0].pageX,
					y:event.changedTouches[0].pageY
				})
			){router.element.style['left'] = '0';}

			if(router.pointOwner({
					x1:window.innerWidth,
					y1:0,
					x2:window.innerWidth,
					y2:window.innerHeight,
					x3:router.point.x,
					y3:router.point.y
				},{
					x:event.changedTouches[0].pageX,
					y:event.changedTouches[0].pageY
				})
			){router.element.style['left'] = '100px';}

        },false);
	}
};
router.init();