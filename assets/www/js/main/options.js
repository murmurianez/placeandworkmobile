var options = {

	openPage:function(){
		"use strict";

		$('.page').hide();
		$('#options').show();

		new IScroll('#options-wrapper');
		document.addEventListener('touchmove',function(event){event.preventDefault()},false);
	}
};