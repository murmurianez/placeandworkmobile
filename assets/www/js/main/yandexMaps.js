var yandexMaps = {
	mainMap:null,
	toWorkMap:null,
	init:function(){
		//main --------------------------------------------------------------
		"use strict";
		$('.information').css({'height':$(window).height() - 40 + 'px'});

		function initMaps(){
			router.mainMap = new ymaps.Map('map',{
				center:[glob.LATITUDE,glob.LONGITUDE],
				zoom:10
			});

			router.toWorkMap = new ymaps.Map("work-describe_map",{
				center: [glob.LATITUDE,glob.LONGITUDE],
				zoom: 14
			});

			var currentPosition = new ymaps.Placemark([glob.LATITUDE,glob.LONGITUDE],{},{preset: 'twirl#greenIcon'});
			router.mainMap.geoObjects.add(currentPosition);

			for(var i = glob.WORKS.length; i--;){
				ymaps.geocode(glob.WORKS[i].address,{results: 1}).then(function(res){
					var workPlacemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(),{},{preset: 'twirl#blueIcon'});
					router.mainMap.geoObjects.add(workPlacemark);
				})
			}
		}
		ymaps.ready(initMaps);


		//workDescribe ------------------------------------------------------
		$('.work-describe-information').css({'height':$(window).height() - 40 + 'px'});

		function initRoute(){
			var id = glob.WORK_TAP_ID.substr(2);
			ymaps.route([
					[glob.LATITUDE,glob.LONGITUDE],
					glob.WORKS[id].address
				]).then(function(route){
					router.toWorkMap.geoObjects.add(route);
				}, function(error){
					console.log('Возникла ошибка: ' + error.message);
				});
		}
		ymaps.ready(initRoute);

		//myProfile ---------------------------------------------------------
		$('.my-profile-information').css({'height':$(window).height() - 40 + 'px'});
	}
};
yandexMaps.init();