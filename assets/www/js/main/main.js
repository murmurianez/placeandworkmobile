var main = {
	mainMap:'',

	openPage:function(){

		"use strict";

		$('.page').hide();
		$('#main').show();
//		document.querySelectorAll(".page").style['display'] = 'none';
//		document.querySelector("#main").style['display'] = 'block';

		$('.information').hammer().on('tap','.information-work',function(){
			glob.WORK_TAP_ID = $(this).attr('id');
//			var template = document.querySelector('#work-describe-template').innerHTML;
			var template = '<p style="font-weight:bold;">' + glob.WORKS[glob.WORK_TAP_ID.substr(2)].prof + '</p>'+
							'<p>' + glob.WORKS[glob.WORK_TAP_ID.substr(2)].description + '</p>'+
							'<p>' + glob.WORKS[glob.WORK_TAP_ID.substr(2)].pay + '</p>'+
							'<img style="width:50px; height:50px; border:1px solid #F7F7F7;" src="" />'+
							'<img style="width:50px; height:50px; border:1px solid #F7F7F7;" src="" />'+
							'<img style="width:50px; height:50px; border:1px solid #F7F7F7;" src="" />'+
							'<img style="width:50px; height:50px; border:1px solid #F7F7F7;" src="" />'+
							'<p>' + glob.WORKS[glob.WORK_TAP_ID.substr(2)].address + '</p>'+
							'<p>' + glob.WORKS[glob.WORK_TAP_ID.substr(2)].employerName + '</p>'+
							'<a href="tel://' + glob.WORKS[glob.WORK_TAP_ID.substr(2)].phone + '">Позвонить ' + glob.WORKS[glob.WORK_TAP_ID.substr(2)].phone + '</a>';
			$('.work-describe-information-work').empty().append(template);
			workDescribe.openPage();
		});

		document.addEventListener("deviceready", function(){
			document.addEventListener("online", onOnline, false);
			document.addEventListener("offline", offOnline, false);

			function onOnline(){$("#network-button").text('Network');}
			function offOnline(){$("#network-button").text('No Internet Access');}
		});

		for(var i = glob.WORKS.length; i--;){
//			var template = document.querySelector('#work-short-describe-template').innerHTML;
			var template =  '<div class="information-work" class="button" data-link="work-describe" id="w-' + i + '">'+
								'<div class="information-left">'+
									'<div class="information-header">' + glob.WORKS[i].prof + '</div>'+
									'<div class="information-content">' + glob.WORKS[i].description + '</div>'+
									'<div class="arrow">'+
										'<img src="img/arrow.png" />'+
									'</div>'+
								'</div>'+
								'<div class="information-right">'+
									'<div class="information-arrow">' + glob.WORKS[i].pay + '</div>'+
									'<div class="information-distance">300м</div>'+
								'</div>'+
							'</div>';
			$('#map').after(template);
		}

		new IScroll('#main-wrapper');
		document.addEventListener('touchmove',function(event){event.preventDefault()},false);
	}

};
