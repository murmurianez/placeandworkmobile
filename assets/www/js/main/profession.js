var profession = {
	toWorkMap:'',

	openPage:function(){
		"use strict";

		$('.page').hide();
		$('#profession').show();

		new IScroll('#profession-wrapper');
		document.addEventListener('touchmove',function(event){event.preventDefault()},false);
	}
};