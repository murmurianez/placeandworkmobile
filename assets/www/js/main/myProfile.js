var myProfile = {
	openPage:function(){
		"use strict";

		$('.page').hide();
		$('#my-profile').show();

		new IScroll('#my-profile-wrapper');
		document.addEventListener('touchmove',function(event){event.preventDefault()},false);
	}
};