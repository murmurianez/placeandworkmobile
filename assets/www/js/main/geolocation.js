var geolocation = {
	getCurrentPosition:function(){
		navigator.geolocation.getCurrentPosition(onSuccess, onError);

		function onSuccess(position){
			glob.LATITUDE = position.coords.latitude;
			glob.LONGITUDE = Math.abs(position.coords.longitude);
		}

		function onError(error){
			console.log('ERROR GEOLOCATION: ' + error);
		}
	}
};