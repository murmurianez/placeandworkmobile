package com.example.PlaceAndWorkMobile;

import android.os.Bundle;
import org.apache.cordova.DroidGap;

public class MyActivity extends DroidGap {
	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		super.setIntegerProperty("splashscreen", R.drawable.splash);
		super.loadUrl("file:///android_asset/www/index.html",5000);
	}
}